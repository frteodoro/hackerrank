import Control.Applicative
import Control.Monad
import System.IO

bubbleSortIteration :: Int -> [Int] -> (Int, [Int])
bubbleSortIteration swaps [] = (swaps, [])
bubbleSortIteration swaps [a] = (swaps, [a])
bubbleSortIteration swaps (a:b:ns)
	| a > b = let (swaps', bubbledTail) = bubbleSortIteration (swaps + 1) (a:ns)
		  in (swaps', b : bubbledTail)	-- swap
	| otherwise = let (swaps', bubbledTail) = bubbleSortIteration swaps (b:ns)
		      in (swaps', a : bubbledTail)
				

bubbleSort :: [Int] -> (Int, [Int])
bubbleSort ls = bubbleSort' 0 ls
	where
		bubbleSort' :: Int -> [Int] -> (Int, [Int])
		bubbleSort' numSwaps ls
			| swaps > 0 = bubbleSort' (numSwaps + swaps) bubbledList
			| otherwise = (numSwaps, ls)
				where (swaps, bubbledList) = bubbleSortIteration 0 ls


main :: IO ()
main = do
    n_temp <- getLine
    let n = read n_temp :: Int
    a_temp <- getLine
    let a = map read $ words a_temp :: [Int]
        (numSwaps, sortedA) = bubbleSort a
    putStrLn $ "Array is sorted in " ++ show numSwaps ++ " swaps."
    putStrLn $ "First Element: " ++ show (head sortedA)
    putStrLn $ "Last Element: " ++ show (last sortedA)

getMultipleLines :: Int -> IO [String]
getMultipleLines n
    | n <= 0 = return []
    | otherwise = do          
        x <- getLine         
        xs <- getMultipleLines (n-1)    
        let ret = (x:xs)    
        return ret          

