import System.IO
import Control.Monad (replicateM_)

isPrime :: Int -> Bool
isPrime 1 = False
isPrime 2 = True
isPrime n
	| n `mod` 2 == 0 = False 
	| otherwise = [x | x <- [3, 5 .. (floor . sqrt . fromIntegral) n], n `mod` x == 0] == []

testPrimality = getLine >>= (\n -> if isPrime n then putStrLn "Prime" else putStrLn "Not prime") . read

main = getLine >>= (\t -> replicateM_ t testPrimality) . read
