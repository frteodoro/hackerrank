import System.IO (getLine, putStrLn)

dec2bin :: Integer -> String
dec2bin 0 = "0"
dec2bin 1 = "1"
dec2bin n 
	| n `mod` 2 == 0 = dec2bin (n `div` 2) ++ "0"
	| n `mod` 2 == 1 = dec2bin (n `div` 2) ++ "1"

maxConsecutive1s :: String -> Integer
maxConsecutive1s = snd . foldl (count1s) (0, 0)
	where	
		count1s (count, maxCount) c
			| c == '0' = (0, maxCount)
			| c == '1' = (count + 1, max maxCount (count + 1))

main = do
	nTmp <- getLine
	let n = read nTmp
	putStrLn $ show $ (maxConsecutive1s . dec2bin) n
