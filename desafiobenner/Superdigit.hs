import System.IO
import Control.Monad
import Data.Char

superDigit :: Int -> String -> String
superDigit 1 xs = xs
superDigit k xs = (superDigit' . show . sum . map digitToInt) $ join $ replicate k (superDigit' xs)
    where superDigit' [x] = [x]
          superDigit' xs = (superDigit' . show . sum . map digitToInt) xs


main :: IO () 
main = do 
    n_temp <- getLine
    let [n, k] = words n_temp
    putStrLn $ superDigit (read k :: Int) n
