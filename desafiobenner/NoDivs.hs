import Control.Applicative
import Control.Monad
import System.IO
import Data.Array

mods k = map (\n -> fromIntegral  (mod n k)) 

modClasses k s = listArray (0, k - 1) (replicate k 0) // [(i, (length . filter (== i)) ms) | i <- [0 .. k - 1]]
    where ms = mods k s

noDivs :: Int -> [Int] -> [Int]
noDivs _ [] = []
noDivs _ [x] = [x]
noDivs k s = zeroRest : evenEl : [val i | i <- [1 .. ceiling (fromIntegral k / 2) - 1]]
    where zeroRest = if mcs!0 > 0 then 1
                                  else 0
          evenEl
            | even k && mcs ! div k 2 > 0 = 1
            | otherwise = 0
          val i = max (mcs!i) (mcs!(k-i))
          mcs = modClasses k s

main :: IO ()
main = do
    n_temp <- getLine
    let [n, k] = map read $ words n_temp :: [Int]
    s_temp <- getLine
    let s = map read $ words s_temp :: [Int]

    --let [n, k] = [4, 3]
    
    --let s = [1, 7, 2, 4, 13, 19]
    --let s = [23, 17]
    --let s = [1 .. 10 ^ 5]

    print $ mods k s
    print $ modClasses k s
    print $ noDivs k s

    print $ sum $ noDivs k s
