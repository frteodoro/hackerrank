import System.IO
import Control.Monad (replicateM, liftM)

type NumberFactors = [(Int, Int)]

myGcd :: NumberFactors -> NumberFactors -> NumberFactors
myGcd ((p, n):l1) l2 = case lookup p l2 of
                          Nothing -> myGcd l1 l2
                          Just n' -> (p, min n n') : myGcd l1 l2
myGcd _ _ = []

lstGcd :: [NumberFactors] -> NumberFactors
lstGcd (l:ls) = foldl myGcd l ls

readNumberFactors :: IO NumberFactors
readNumberFactors = liftM (conv . map read . words) getLine
    where conv (p:n:fs) = (p, n) : conv fs
          conv _ = []

main = readLn >>= flip replicateM readNumberFactors >>= putStrLn . unwords . map (\(p, n) -> show p ++ " " ++ show n) . lstGcd
