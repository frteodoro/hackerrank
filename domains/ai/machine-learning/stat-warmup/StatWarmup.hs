import Data.List
import Data.Map (fromListWith, toList)
import Data.Maybe (fromJust)
import Text.Printf

mean :: Int -> [Int] -> Double
mean n a = fromIntegral (sum a) / fromIntegral n

median :: Int -> [Int] -> Double
median n a
    | even n = mean 2 [sort a !! (n `div` 2), sort a !! ((n `div` 2) - 1)]
    | odd  n = fromIntegral $ sort a !! (n `div` 2)

mode :: [Int] -> Double
mode a = fromIntegral $ fromJust $ lookup maxFreq freqs
    where 
          freqs = sort [(y, x) | (x, y) <- frequency a]
          maxFreq = fst $ maximum freqs
          frequency xs = toList (fromListWith (+) [(x, 1) | x <- xs])

stdDev :: Int -> [Int] -> Double
--stdDev n a = sqrt $ fromIntegral $ sum $ map (\x -> (x - m) ^ 2) a
stdDev n a = sqrt $ sum (map (\x -> (fromIntegral x - m) ^ 2) a) / fromIntegral n
    where
        m = mean n a

main = do
    n <- readLn :: IO Int
    a <- (map read . words) <$> getLine :: IO [Int]
    printf "%.1f\n" $ mean n a
    printf "%.1f\n" $ median n a
    printf "%.0f\n" $ mode a
    printf "%.1f\n" $ stdDev n a
