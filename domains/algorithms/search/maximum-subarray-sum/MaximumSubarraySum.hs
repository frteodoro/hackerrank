maxSubarraySum :: Integer -> Integer -> Integer -> [Integer] -> Integer
maxSubarraySum modulus soFar endingHere [] = max soFar endingHere `mod` modulus
maxSubarraySum modulus soFar endingHere (x:xs) =
    let endingHere'
            | (x `mod` modulus) > (endingHere + x) `mod` modulus = x `mod` modulus
            | otherwise = (endingHere + x) `mod` modulus
        soFar'
            | endingHere' > soFar = endingHere'
            | otherwise = soFar
    in maxSubarraySum modulus soFar' endingHere' xs

doQueries :: Integer -> IO ()
doQueries 0 = return ()
doQueries q = do
    l1 <- getLine
    l2 <- getLine
    let [n, m] = map read . words $ l1
        a = (map read . words) l2
    print $ maxSubarraySum m 0 0 a
    doQueries (q-1)

main = do
    q <- readLn
    doQueries q

