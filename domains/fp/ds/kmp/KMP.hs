piTable :: String -> [Int]
piTable p = let t = 0:[x | i <- [1 .. length p - 1], let x = mkPi p t i (t!!(i-1))]
                 in t

mkPi :: String -> [Int] -> Int -> Int -> Int
mkPi p t i prevQ
    | p!!prevQ == p!!i = prevQ + 1
    | otherwise = recursion p t i (t!!prevQ)

recursion :: String -> [Int] -> Int -> Int -> Int
recursion p t i q
    | q == 0 = 0
    | p!!(q-1) == p!!i = q
    | otherwise = recursion p t i (t!!(q-1))

--matcher :: String -> String -> Bool
matcher t p = 
    let n = length t
        m = length p
        pi = piTable p
        qs = 0:[q | i <- [1 .. n], let q = match t p pi (i-1) (qs!!(i-1))]
    --in elem m qs
    in qs

match :: String -> String -> [Int] -> Int -> Int -> Int
match t p pi i q
    | q == m = pi!!(q-1)
    | p!!q == t!!i = q + 1
    | otherwise = recurse t p pi i (pi!!q)
    where recurse t p pi i q
              | q < 0 = 0
              | t!!i == p!!q = q + 1
              | otherwise = recurse t p pi i (pi!!(q-1))
          m = length p

main = undefined
