import Control.Monad

data BinaryTree = Node Int BinaryTree BinaryTree | Empty
                deriving Show

-- Inorder traversal is performed as
--
--   1. Traverse the left subtree.
--   2. Visit root (put its data in the list).
--   3. Traverse the right subtree.
inorderTraverse :: BinaryTree -> [Int]
inorderTraverse Empty = []
inorderTraverse (Node x l r) 
    = inorderTraverse l ++ [x] ++ inorderTraverse r

swapNodes :: BinaryTree -> BinaryTree
swapNodes (Node x l r) = Node x r l
swapNodes Empty = Empty

swapOp :: BinaryTree -> Int -> BinaryTree
swapOp t k = swapOp' t 1
    where swapOp' (Node x l r) d
              | d `mod` k == 0 = Node x sr sl
              | otherwise      = Node x sl sr
                where sl = swapOp' l (d+1)
                      sr = swapOp' r (d+1)
          swapOp' Empty _ = Empty

mkTree :: [Int] -> BinaryTree
mkTree idxs = mkTree' (0:1:idxs) 1

mkTree' :: [Int] -> Int -> BinaryTree
mkTree' idxs i 
    | i > length idxs - 1 = Empty
    | idxs!!i == -1       = Empty
    | otherwise           = Node (idxs!!i) (mkTree' idxs (i*2)) (mkTree' idxs (2*i+1))

main = do
    n <- readLn
    inputs <- replicateM n ((map read . words) <$> getLine)
    print (join inputs :: [Int])
    print $ inorderTraverse $ mkTree (join inputs)
    --idxs <- replicateM n (readLn >>= (return . map read . words))
    --print (join idxs :: [Int])
