import Control.Monad

fib = 0:1:1:[fib !! (i-2) + fib !! (i-1) | i <- [3..]]

main = 
    readLn >>= flip replicateM_ (readLn >>= print . (\x -> (fib !! x) `mod` (10^8+7)))
