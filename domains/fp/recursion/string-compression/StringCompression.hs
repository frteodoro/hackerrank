stringCompressed :: String -> String
stringCompressed [] = []
stringCompressed [a] = [a]
stringCompressed (a:b:rest)
    | a == b = a : show n ++ stringCompressed (drop (n-1) (b:rest))
    | otherwise = a : stringCompressed (b:rest)
    where countChars count [] = count
          countChars count [x] = count+1
          countChars count (x:y:zs)
              | x == y = countChars (count+1) (y:zs)
              | otherwise = count+1
              
          n = countChars 1 (b:rest)

main = do
    str <- getLine
    putStrLn $ stringCompressed str
