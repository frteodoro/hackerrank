prefix :: String -> String -> String
prefix [] _ = []
prefix _ [] = []
prefix (x:xs) (y:ys)
    | x == y = x : prefix xs ys
    | otherwise = []

main = do
    x <- getLine
    y <- getLine
    let p = prefix x y
        x' = drop (length p) x
        y' = drop (length p) y
    putStrLn $ show (length p) ++ " " ++ p
    putStrLn $ show (length x') ++ " " ++ x'
    putStrLn $ show (length y') ++ " " ++ y'
