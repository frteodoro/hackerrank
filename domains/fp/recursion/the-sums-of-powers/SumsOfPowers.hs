-- Find the number of ways that a given integer, X, can be expressed as the sum
-- of the N'th power of unique, natural numbers.

import System.IO
import Control.Monad (guard)

sumsOfPowers :: Int -> Int -> [Int] -> [Int] -> [[Int]]
sumsOfPowers n x xs acc
    | x <  0 = []
    | x == 0 = [acc]
    | otherwise = concatMap helper . filter ((<= x) . (^n)) $ xs
    where 
          helper x' = sumsOfPowers n (x - x'^n) (filter (>x') xs) (x':acc)

main = do
    x <- readLn
    n <- readLn
    print $ length $ sumsOfPowers n x [1 .. (floor . sqrt) (fromIntegral x)] []
