import Data.Bits (testBit)

-- Tree representation
newtype TreeData = TreeData Bool

instance Show TreeData where
    show (TreeData False) = "."
    show (TreeData True) = "X"

instance Read TreeData where
    readsPrec _ (' ':s) = reads s
    readsPrec _ ('.':s) = [(TreeData False, s)]
    readsPrec _ ('X':s) = [(TreeData True, s)]
    readsPrec _ _ = []

data Tree = Leaf TreeData | Tree TreeData Tree Tree

instance Show Tree where
    show (Leaf x) = show x
    show (Tree x l r) = "(" ++ show l ++ " " ++ show x ++ " " ++ show r ++ ")"

readsTree   :: ReadS Tree
readsTree s = [(Tree d l r, x) | ("(", t) <- lex s,
                                 (l,   u) <- readsTree t, 
                                 (d,   v) <- reads u,
                                 (r,   w) <- readsTree v,
                                 (")", x) <- lex w         ]
              ++
              [(Leaf x, t)  | (x,   t) <- reads s       ]

instance Read Tree where
    readsPrec _ = readsTree


-- Rule representation

type Rule = [((Bool, Bool, Bool, Bool), Bool)]

intToRule :: Int -> Rule
intToRule n = zip [(u, l, x, r) 
                                | u <- [False, True],  
                                  l <- [False, True], 
                                  x <- [False, True], 
                                  r <- [False, True]] 
                  [testBit n i | i <- [0 .. 15]]

main = undefined
